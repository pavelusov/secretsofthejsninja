// function Book() {
//
// }
function showPrice(title, price) {
    console.log('Это книга: ' + this[title] + '. Цена: ' + this[price]);
    console.log(this);
}
var book = {
    titleBook : 'JS Forever',
    priceBook : 1000,
    author : 'Flenagan'
};
var product = ['port', 'austria', 200];
// console.log(book.priceBook);
showPrice.call(book, 'titleBook', 'priceBook');
showPrice.apply(book, ['titleBook', 'author']);